﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DCE.Hertz2.Models
{
    public class Invoice
    {
        public int InvoiceId { get; set; }
        public int? CustomerId { get; set; }
        [Display(Name="Invoice Number")]
        public string InvoiceNumber { get; set; }
        public string Account { get; set; }
        public string ChargeTo { get; set; }
        public string Address2 { get; set; }
        
        [DisplayFormat(DataFormatString="{0:d-MMM-yy}")]
        [Display(Name="Invoice Date")]
        public DateTime? InvoiceDate { get; set; }
        public string GSTNumber { get; set; }
        public decimal? GSTAmount { get; set; }
        public string Reference { get; set; }
        public string Reservation { get; set; }
        public string OrderNumber { get; set; }
        public string VoucherNumber { get; set; }
        public string FreqFlyer { get; set; }
        public string PayType { get; set; }
        public string RateCDP { get; set; }
        public string Hirer { get; set; }
        public string PickUpLocation { get; set; }

        [DisplayFormat(DataFormatString = "{0:d-MMM-yy}")]
        public DateTime? PickUpDateTime { get; set; }
        public string ReturnLocation { get; set; }
        [DisplayFormat(DataFormatString = "{0:d-MMM-yy}")]
        public DateTime? ReturnDateTime { get; set; }
        public string LicenceNo { get; set; }
        public string VehicleCharged { get; set; }
        public string ReturnVehicle { get; set; }
        public int? KMOut { get; set; }
        public int? KMIn { get; set; }
        public int? CarbonEmissions { get; set; }
        public string Currency { get; set; }
        [DisplayFormat(NullDisplayText = "0.00")]
        public decimal? TotalChargesPaid { get; set; }


        public int?  KMDriven
        {
            get
            {
                if (KMIn.HasValue && KMOut.HasValue)
                {
                    return KMIn.Value - KMOut.Value;
                }
                return 0;
            }
            
        }

        public Customer Customer { get; set; }
        public virtual ICollection<Charges> Charges { get; set; }

    }
}