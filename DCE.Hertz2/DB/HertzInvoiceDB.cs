﻿using DCE.Hertz2.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace DCE.Hertz2.DB
{
    public class HertzInvoiceDB : DbContext
    {
        public HertzInvoiceDB()
            : base("DefaultConnection")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }

        
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
    }
}
//PM > Add-Migration InitialCreate
//PM > Enable-Migrations -ContextTypeName HertzInvoiceDb -EnableAutomaticMigrations
//PM > Update-Database -Verbose -Force
