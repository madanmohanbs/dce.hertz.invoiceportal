﻿using DCE.Hertz2.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace DCE.Hertz2.Utilities
{

    public class ReadFile
    {
        static internal Dictionary<string, InvoiceData> CollectInvoice(string file)
        {
            var collection = new Dictionary<string, InvoiceData>();

            StreamReader reader = new StreamReader(file);

            int index = 0;
            string[] headings = new string[] { };
            while (!reader.EndOfStream)
            {
                var invoice = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

                if (index == 0)
                {
                    var header = reader.ReadLine();
                    headings = header.Split(',');

                }
                index++;
                var line = reader.ReadLine();
                var items = line.Split(',');

                int itemCount = headings.Length;
                for (int i = 0; i < itemCount; i++)
                {
                    if (string.Empty != (headings[i]))
                    {
                        var key = String.Join("", headings[i].Split(' ', ':', '&', '-', ';', '/'));
                        invoice.Add(key, items[i]);
                    }
                }


                string invoiceNumber = invoice["InvoiceNumber"];
                var itemkeys = new List<string> { "RentalCharges", "Quantity", "Per", "Rate", "Subtotal" };
                var invoiceItems = itemkeys.Where(k => invoice.ContainsKey(k)).Select(k => new KeyValuePair<string, string>(k, invoice[k])).ToDictionary(k => k.Key, k => k.Value,StringComparer.InvariantCultureIgnoreCase);


                if (!collection.ContainsKey(invoiceNumber))
                {
                    InvoiceData data = new InvoiceData { InvoiceDetail = invoice };
                    data.InvoiceItems.Add(invoiceItems);
                    collection.Add(invoiceNumber, data);
                }
                else
                {
                    InvoiceData data = collection[invoiceNumber];
                    data.InvoiceItems.Add(invoiceItems);
                }


            }
            return collection;
        }



        static internal Dictionary<string, CustomerData> CollectCustomer(string file)
        {
            var collection = new Dictionary<string, CustomerData>();

            StreamReader reader = new StreamReader(file);

            int index = 0;
            string[] headings = new string[] { };
            while (!reader.EndOfStream)
            {
                var customer = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

                if (index == 0)
                {
                    var header = reader.ReadLine();
                    headings = header.Split(',');

                }
                index++;
                var line = reader.ReadLine();
                var items = line.Split(',');

                int itemCount = headings.Length;
                for (int i = 0; i < itemCount; i++)
                {
                    if (string.Empty != (headings[i]))
                    {
                        var key = String.Join("", headings[i].Split(' ', ':', '&', '-', ';', '/'));
                        customer.Add(key, items[i]);
                    }

                }

                string accountNumber = customer["Account"];
                if (!collection.ContainsKey(accountNumber))
                {
                    CustomerData data = new CustomerData { Detail = customer };
                    collection.Add(accountNumber, data);
                }

            }
            return collection;
        }


    }


    class InvoiceData
    {

        public Dictionary<string, string> InvoiceDetail { get; set; }
        public List<Dictionary<string, string>> InvoiceItems { get; set; }

        public InvoiceData()
        {
            InvoiceItems = new List<Dictionary<string, string>>();
        }

    }

    class CustomerData
    {
        public Dictionary<string, string> Detail { get; set; }
    }
}