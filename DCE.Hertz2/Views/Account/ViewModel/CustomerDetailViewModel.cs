﻿using DCE.Hertz2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCE.Hertz2.ViewModel
{
    public class CustomerDetailViewModel
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string PayType { get; set; }
        public string Status { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public ICollection<Invoice> Invoices { get; set; }
    }

    public static partial class ExtenstionMehods
    {
        public static CustomerDetailViewModel ToDetailViewModel(this Customer model)
        {
            return new CustomerDetailViewModel
            {
                Id = model.Id,
                Account = model.Account,
                Name = model.Name,
                Company = model.Company,
                PayType = model.PayType,
                Status=model.Status,
                Address1=model.Address1,
                Address2=model.Address2,
                City = model.City,
                State = model.State,
                Zip = model.Zip,
                Country = model.Country,
                Email = model.Email,
                Phone = model.Phone,

                Invoices = model.Invoices
            };

        }

        public static Customer ToModel(this CustomerDetailViewModel model)
        {
            return new Customer
            {
                Id = model.Id,
                Account = model.Account,
                Name = model.Name,
                Company = model.Company,
                PayType = model.PayType,
                Status = model.Status,
                Address1 = model.Address1,
                Address2 = model.Address2,
                City = model.City,
                State = model.State,
                Zip = model.Zip,
                Country = model.Country,
                Email = model.Email,
                Phone = model.Phone

            };

        }

        public static IEnumerable<CustomerDetailViewModel> ToDetailViewModel(this List<Customer> list)
        {
            foreach (var model in list)
            {
                yield return model.ToDetailViewModel();
            }
        }

        public static IEnumerable<Customer> ToViewModel(this List<CustomerDetailViewModel> list)
        {
            foreach (var model in list)
            {
                yield return model.ToModel();
            }
        }
    }
}