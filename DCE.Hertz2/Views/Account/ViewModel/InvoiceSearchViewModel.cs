﻿using DCE.Hertz2.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCE.Hertz2.ViewModel
{
    public class InvoiceSearchViewModel
    {
        public Customer Customer { get; set; }
        public IPagedList<Invoice> Invoices { get; set; }
    }
}