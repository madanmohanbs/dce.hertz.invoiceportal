﻿using DCE.Hertz2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCE.Hertz2.ViewModel
{
    public class CustomerListViewModel
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string PayType { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        
        

        
    }

    

    public static partial class ExtenstionMehods
    {
        public static CustomerListViewModel ToViewModel(this Customer model)
        {
            return new CustomerListViewModel
            { 
                Id = model.Id,
                Account = model.Account,
                Name = model.Name,
                Company = model.Company,
                PayType = model.PayType,
                City = model.City,
                Country = model.Country
            };

        }

        public static Customer ToModel(this CustomerListViewModel model)
        {
            return new Customer
            {
                Id = model.Id,
                Account = model.Account,
                Name = model.Name,
                Company = model.Company,
                PayType = model.PayType,
                City = model.City,
                Country = model.Country

            };

        }

        public static IEnumerable<CustomerListViewModel> ToViewModel(this List<Customer> list)
        {
            foreach (var model in list)
            {
                yield return model.ToViewModel();
            }
        }

        public static IEnumerable<Customer> ToViewModel(this List<CustomerListViewModel> list)
        {
            foreach (var model in list)
            {
                yield return model.ToModel();
            }
        }
    }
}