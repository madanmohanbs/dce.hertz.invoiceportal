﻿using DCE.Hertz2.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DCE.Hertz2.ViewModel
{
    public class InvoiceListViewModel
    {
        public int InvoiceId { get; set; }

        [DisplayFormat(DataFormatString = "{0:d-MMM-yy}")]
        [Display(Name = "Invoice Date")]
        public DateTime? InvoiceDate { get; set; }
        
        [Display(Name = "Rental Agreement")]
        public string RentalAgreement { get; set; }

        public string Reservation { get; set; }

        [DisplayFormat(DataFormatString = "{0:d-MMM-yy}")]
        [Display(Name = "Rental Date")]
        public DateTime? RentalDate { get; set; }

        public string Location { get; set; }
        public string Hirer { get; set; }
        public string Account { get; set; }
        
        [Display(Name = "Account Name")]
        public string AccountName { get; set; }
       
        [Display(Name = "Referred By")]
        public string ReferredBy { get; set; }

        public string Reference { get; set; }
        public decimal Balance { get; set; }


    }

    public static partial class ExtenstionMehods
    {
        public static InvoiceListViewModel ToViewModel(this Invoice model)
        {
            return new InvoiceListViewModel
            {
                InvoiceId = model.InvoiceId,
                InvoiceDate = model.InvoiceDate,
                RentalAgreement = model.OrderNumber,
                Reservation = model.Reservation,
                RentalDate = model.PickUpDateTime,
                Location = model.PickUpLocation,
                Hirer = model.Hirer,
                Account = model.Account,
                AccountName = "Need Linq",
                ReferredBy = "???",
                Balance = 999.99M

            };

        }

        //public static Invoice ToModel(this InvoiceListViewModel model)
        //{
        //    return new Invoice
        //    {
        //        InvoiceId = model.InvoiceId,
        //    };
        //}

        public static IEnumerable<InvoiceListViewModel> ToViewModel(this List<Invoice> list)
        {
            foreach (var model in list)
            {
                yield return model.ToViewModel();
            }
        }

        //public static IEnumerable<Invoice> ToViewModel(this List<InvoiceListViewModel> list)
        //{
        //    foreach (var model in list)
        //    {
        //        yield return model.ToModel();
        //    }
        //}
    }
}