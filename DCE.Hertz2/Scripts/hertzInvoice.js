﻿$(function () {
    //AJAX
    var ajaxFormSubmit = function () {
        var $form = $(this)

        var options = {
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize(),
        };

        $.ajax(options).done(function (data) {
            var $target = $($form.attr("data-zzz-target"));
            $target.replaceWith(data);
        });
        return false;
    };

   
    //AUTO COMPLETE
    var createAutocomplete = function () {
        var $input = $(this);

        var options = {
            source: $input.attr("data-zzz-autocomplete")
        };

        $input.autocomplete(options);
    }

    
    //PAGING
    var getPage = function () {
        var $a = $(this);
        
        var options = {
            url: $a.attr("href"),
            data: $("form").serialize(),
            type: "get"
        };
        $.ajax(options).done(function (data) {
            var target = $a.parents("div.pagedList").attr("data-zzz-target");
           
            $(target).replaceWith(data);
        }).fail(function (xhr,status,error) {
            //alert(xhr.responseText)
        });
        return false;
    }

    //BIND AJAX
    $("form[data-zzz-ajax ='true']").submit(ajaxFormSubmit);

    //BIND AUTO COMPLETE
    $("input[data-zzz-autocpmplete]").each(createAutocomplete);

    //BIND PAGER
    $(".body-content").on("click", ".pagedList a", getPage);

});