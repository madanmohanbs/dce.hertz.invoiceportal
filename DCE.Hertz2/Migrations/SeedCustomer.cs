﻿using DCE.Hertz2.DB;
using DCE.Hertz2.Models;
using DCE.Hertz2.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;


namespace DCE.Hertz2.Migrations
{
    public static class SeedCustomer
    {
        public static void Seed(HertzInvoiceDB context, string csvfilepath)
        {

            var customers = ReadFile.CollectCustomer(csvfilepath);
            foreach (var cust in customers.Values.ToList()) //.Take(100))
            {
                var customer = cust.Detail;

                var detail = new Customer
                    {
                        Account = customer.ContainsKey("Account") ? customer["Account"] : null,
                        Name = customer.ContainsKey("Hirer") ? customer["Hirer"] : null,
                        PayType = customer.ContainsKey("Paytype") ? customer["Paytype"] : null,
                        Status = customer.ContainsKey("Status") ? customer["Status"] : null,
                        Address2 = customer.ContainsKey("Address2") ? customer["Address2"] : null,
                        City = customer.ContainsKey("City") ? customer["City"] : null,
                        State = customer.ContainsKey("State") ? customer["State"] : null,
                        Zip = customer.ContainsKey("Zip") ? customer["Zip"] : null,
                        Country = customer.ContainsKey("Country") ? customer["Country"] : null,
                        Phone = customer.ContainsKey("Telephone") ? customer["Telephone"] : null,
                        Email = customer.ContainsKey("Email") ? customer["Email"] : null
                    };

                if (context.Customers.Where(c => c.Account == detail.Account && c.Email == detail.Email).Count() == 0)
                {
                    context.Customers.AddOrUpdate(c => c.Id, detail);
                }
            }

            context.SaveChanges();

        }
    }
}