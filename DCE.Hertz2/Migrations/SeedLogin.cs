﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;

namespace DCE.Hertz2.Migrations
{
    public static class SeedLogin
    {
        public static void Seed()
        {
            DCE.Hertz2.AuthConfig.SimpleMembershipInitializer _initializer = null;
            object _initializerLock = new object();
            bool _isInitialized = false;
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);

            //The Role Manager feature has not been enabled. --> web.config +roleManager & +membership
            if (!WebSecurity.Initialized)
            {
                WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: false);
            }
            //WebSecurity.
            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            if (!roles.RoleExists("Admin"))
            {
                roles.CreateRole("Admin");
            }

            if (membership.GetUser("admin", false) == null)
            {
                membership.CreateUserAndAccount("admin", "admin");
            }
            if (!roles.GetRolesForUser("admin").Contains("Admin"))
            {
                roles.AddUsersToRoles(new[] { "admin" }, new[] { "Admin" });
            }

            //Users
            if (!roles.RoleExists("User"))
            {
                roles.CreateRole("User");
            }

        }
    }
}