﻿using DCE.Hertz2.DB;
using DCE.Hertz2.Utilities;
using System.Linq;
using System.Web.Security;
using WebMatrix.WebData;

namespace DCE.Hertz2.Migrations
{
    public class SeedUser
    {
        public static void Seed(HertzInvoiceDB context, string csvfilepath)
        {

            var roles = (SimpleRoleProvider)Roles.Provider;

            var userProfiles = ReadFile.CollectCustomer(csvfilepath);
            var customerTable = context.Customers.ToDictionary(k => k.Account, k => k.Id);

            foreach (var userProfile in userProfiles.Values.ToList()) //.Take(100))
            {
                var customer = userProfile.Detail;
                string Account = customer.ContainsKey("Account") ? customer["Account"] : null;
                string Hirer = customer.ContainsKey("Hirer") ? customer["Hirer"] : null;
                string Email = customer.ContainsKey("Email") ? customer["Email"] : null;
                int? CustomerId = customerTable.ContainsKey(Account) ? customerTable[Account] : (int?)null;

                string UserName = string.IsNullOrEmpty(Email) ? Account : Email;
                string Password = Account;

                if (!WebSecurity.UserExists(UserName))
                {
                    WebSecurity.CreateUserAndAccount(UserName, Password, propertyValues: new { Account, Email, CustomerId });
                }

                if (!roles.IsUserInRole(UserName, "User"))
                {
                    roles.AddUsersToRoles(new[] { UserName }, new[] { "User" });
                }
            }
        }
    }
}