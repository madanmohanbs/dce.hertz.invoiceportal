﻿using DCE.Hertz2.DB;
using DCE.Hertz2.Models;
using DCE.Hertz2.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace DCE.Hertz2.Migrations
{
    public static class SeedInvoice
    {
        public static void Seed(HertzInvoiceDB context, string csvfilepath)
        {

            var collection = ReadFile.CollectInvoice(csvfilepath);
            var list = collection.Values.ToList(); // .Take(200);

            var customers = context.Customers.Select(c => new { c.Account, c.Id }).ToDictionary(c => c.Account, c => c.Id, StringComparer.InvariantCultureIgnoreCase);

            Console.WriteLine(customers.Count());

            List<Invoice> all = new List<Invoice>();
            foreach (var data in list)
            {
                var invoice = data.InvoiceDetail;
                var items = data.InvoiceItems;
                DateTime time;
                int number;
                decimal money;
                string account = invoice.ContainsKey("Account") ? invoice["Account"] : null;
                //TODO :make rubust in key lookup ,   handle null when converting type
                var detail = new Invoice
                {
                    //CustomerId = null,
                    InvoiceNumber = invoice.ContainsKey("InvoiceNumber") ? invoice["InvoiceNumber"] : null,
                    Account = account,
                    CustomerId = customers.ContainsKey(account) ? customers[account] : (int?)null,
                    ChargeTo = invoice.ContainsKey("ChargeTo") ? invoice["ChargeTo"] : null,
                    Address2 = invoice.ContainsKey("Address2") ? invoice["Address2"] : null,
                    InvoiceDate = invoice.ContainsKey("InvoiceDate") ? DateTime.TryParse(invoice["InvoiceDate"], out time) ? time : DateTime.Now : (DateTime?)null,
                    GSTNumber = invoice.ContainsKey("GSTNo") ? invoice["GSTNo"] : null,
                    Reference = invoice.ContainsKey("Reference") ? invoice["Reference"] : null,
                    Reservation = invoice.ContainsKey("Reservation") ? invoice["Reservation"] : null,
                    OrderNumber = invoice.ContainsKey("OrderNumber") ? invoice["OrderNumber"] : null,
                    VoucherNumber = invoice.ContainsKey("VoucherNumber") ? invoice["VoucherNumber"] : null,
                    FreqFlyer = invoice.ContainsKey("FreqFlyer") ? invoice["FreqFlyer"] : null,
                    PayType = invoice.ContainsKey("Paytype") ? invoice["Paytype"] : null,
                    RateCDP = invoice.ContainsKey("RateCDP") ? invoice["RateCDP"] : null,
                    Hirer = invoice.ContainsKey("Hirer") ? invoice["Hirer"] : null,
                    PickUpLocation = invoice.ContainsKey("PickUpLocation") ? invoice["PickUpLocation"] : null,
                    PickUpDateTime = invoice.ContainsKey("PickUpDateTime") ? DateTime.TryParse(invoice["PickUpDateTime"], out time) ? time : DateTime.Now : (DateTime?)null,
                    ReturnLocation = invoice.ContainsKey("ReturnLocation") ? invoice["ReturnLocation"] : null,
                    ReturnDateTime = invoice.ContainsKey("ReturnDateTime") ? DateTime.TryParse(invoice["ReturnDateTime"], out time) ? time : DateTime.Now : (DateTime?)null,
                    VehicleCharged = invoice.ContainsKey("VehicleCharged") ? invoice["VehicleCharged"] : null,
                    LicenceNo = invoice.ContainsKey("LicenceNo") ? invoice["LicenceNo"] : null,
                    ReturnVehicle = invoice.ContainsKey("ReturnVehicle") ? invoice["ReturnVehicle"] : null,
                    KMOut = invoice.ContainsKey("KMOut") ? int.TryParse(invoice["KMOut"], out number) ? number : 0 : (int?)null,
                    KMIn = invoice.ContainsKey("KMIn") ? int.Parse(invoice["KMIn"]) : (int?)null,
                    CarbonEmissions = invoice.ContainsKey("CarbonEmissions") ? int.TryParse(invoice["CarbonEmissions"], out number) ? number : 0 : (int?)null,
                    GSTAmount = invoice.ContainsKey("GSTAmount") ? decimal.TryParse(invoice["GSTAmount"], out money) ? money : 0M : (decimal?)null,
                    Currency = invoice.ContainsKey("Currency") ? invoice["Currency"] : null,

                    Charges = new List<Charges>()
                };


                foreach (var item in items)
                {

                    detail.Charges.Add(
                      new Charges
                      {
                          //InvoiceId = detail.InvoiceId,
                          RentalCharges = item["RentalCharges"],
                          Quantity = item.ContainsKey("Quantity") ? decimal.TryParse(item["Quantity"], out money) ? money : 0M : (decimal?)null,
                          Per = item["Per"],
                          Rate = item.ContainsKey("Rate") ? decimal.TryParse(item["Rate"], out money) ? money : 0M : (decimal?)null,
                          SubTotal = item.ContainsKey("Subtotal") ? decimal.TryParse(item["Subtotal"], out money) ? money : 0M : (decimal?)null,

                      }
                    );
                }

                if (context.Invoices.Where(i => i.InvoiceNumber == detail.InvoiceNumber).Count() == 0)
                {
                    context.Invoices.AddOrUpdate(v => v.InvoiceId, detail);
                }

            }
            context.SaveChanges();
        }
    }
}