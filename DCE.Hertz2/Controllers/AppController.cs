﻿using DCE.Hertz2.Migrations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DCE.Hertz2.Controllers
{
    //[Authorize(Roles = "Admin")]
    public class AppController : Controller
    {
        //
        // GET: /App/

        public ContentResult Migrate()
        {
            using (DCE.Hertz2.DB.HertzInvoiceDB context = new DB.HertzInvoiceDB())
            {
                SeedLogin.Seed();

                //The process cannot access the file 'C:\Dhamy\nz_account.csv' because it is being used by another process.

                string SetupFilePath = ConfigurationManager.AppSettings["SetupFilePath"];
                string InvoicesCsvFileName = ConfigurationManager.AppSettings["InvoicesCsvFileName"];
                string AccountsCsvFileName = ConfigurationManager.AppSettings["AccountsCsvFileName"];

                string invoicefile = Server.MapPath(SetupFilePath + InvoicesCsvFileName);
                string accountfile= Server.MapPath(SetupFilePath + AccountsCsvFileName);
               
                SeedCustomer.Seed(context,accountfile );
                SeedInvoice.Seed(context, invoicefile);
                SeedUser.Seed(context, accountfile);
            }

            return Content("success");
        }
        public ContentResult ConnectionString()
        {
            string conn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            return Content(conn);
        }

    }
}
