﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DCE.Hertz2.DB;
using DCE.Hertz2.Models;
using DCE.Hertz2.ViewModel;

namespace DCE.Hertz2.Controllers
{
    [Authorize(Roles="Admin")]
    public class CustomerController : Controller
    {
        private HertzInvoiceDB db = new HertzInvoiceDB();
        private UsersContext userDb = new UsersContext();

       

       
        
        #region Auto generated    
        public ActionResult Index()
        {
            //var customers = db.Customers.Join(db.UserProfiles, c => c.Id, u => u.CustomerId,
            //    (c, u) => new Customer
            //    {
            //        Id = c.Id,
            //        Account = c.Account,
            //        Name = c.Name,
            //        Company = c.Company,
            //        PayType = c.PayType,
            //        City = c.City,
            //        UsersCount = c.UserProfiles.Count()
            //    });
            //return View(customers.ToList());
            var customers = db.Customers.Include(c=>c.UserProfiles).Include(c=>c.Invoices);
            foreach (var customer in customers)
            {
                customer.UsersCount = customer.UserProfiles.Count();
            }
            return View(customers);
        }
            
      
        public ActionResult Detail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer.ToDetailViewModel());
        }
       
        // GET: Customer/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Account, Name, Company, PayType, City, Country")] CustomerListViewModel customer)
        {
            if (ModelState.IsValid)
            {
                db.Customers.Add(customer.ToModel());
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(customer);
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer.ToViewModel());
        }

        // POST: Customer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Account, Name, Company, PayType, City, Country")] CustomerListViewModel customer)
        {
            var entity = customer.ToModel();
            if (ModelState.IsValid)
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customer);
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer.ToViewModel());
        }

        // POST: Customer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Customer customer = db.Customers.Find(id);
            db.Customers.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                userDb.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
