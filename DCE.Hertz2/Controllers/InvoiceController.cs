﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DCE.Hertz2.Models;
using DCE.Hertz2.DB;
using DCE.Hertz2.ViewModel;
using DCE.Hertz2.Filters;
using System.Configuration;
using PagedList;

namespace DCE.Hertz2.Controllers
{
    [Authorize]
    public class InvoiceController : Controller
    {
        private HertzInvoiceDB db = new HertzInvoiceDB();
        private UsersContext userDb = new UsersContext();

        public ActionResult Autocomplete(string term)
        {
            var model = db.Invoices.Where(i => i.InvoiceNumber.StartsWith(term))
                .Take(10)
                .Select(i => new
                {
                    label = i.InvoiceNumber
                });
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "User")]
        public ActionResult Index(string searchKey, string searchTerm, int page = 1)
        {
            string account = userDb.UserProfiles.Where(u => u.UserName == User.Identity.Name).Select(u => u.Account).FirstOrDefault();
            Customer customer = db.Customers.Where(c => c.Account == account).FirstOrDefault();
            var invoices = db.Invoices.Where(i => i.Customer.Account == account && (searchTerm == null || i.InvoiceNumber.StartsWith(searchTerm)))
                .OrderByDescending(i => i.InvoiceDate).ToPagedList(page, 10);

            var model = new ViewModel.InvoiceSearchViewModel { Customer = customer, Invoices = invoices };
            if (model == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
            {
                return PartialView("_InvoiceSearchResult", invoices);
            }

            return View(model);

        }

        #region Auto generated
        //[Authorize(Roles="User")]
        //public ActionResult Index()
        //{
        //    string account = userDb.UserProfiles.Where(u=> u.UserName ==  User.Identity.Name).Select(u=>u.Account).FirstOrDefault();

        //    //return View(db.Invoices.ToList());
        //    return View(db.Invoices.Where(i => i.Account == account).ToList());
        //}
        public ActionResult List()
        {
            return View(db.Invoices.ToList().ToViewModel());
        }


        public ActionResult Details(int id = 0)
        {
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        public ActionResult Download(int id = 0)
        {
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            string InvoicePdfFilePath = ConfigurationManager.AppSettings["InvoicePdfFilePath"];
            string invoicePdfpath = string.Format(@"{0}\{1}.pdf", InvoicePdfFilePath, invoice.InvoiceNumber);
            return File(invoicePdfpath, "application/octet-stream", invoice.InvoiceNumber + ".pdf"); //application/pdf

        }
        public ActionResult View(int id = 0)
        {
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            string InvoicePdfFilePath = ConfigurationManager.AppSettings["InvoicePdfFilePath"];
            string invoicePdfpath = string.Format(@"{0}\{1}.pdf", InvoicePdfFilePath, invoice.InvoiceNumber);
            return File(invoicePdfpath, "application/pdf");

        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                db.Invoices.Add(invoice);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(invoice);
        }

        public ActionResult Edit(int id = 0)
        {
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(invoice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(invoice);
        }


        public ActionResult Delete(int id = 0)
        {
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }



        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Invoice invoice = db.Invoices.Find(id);
            db.Invoices.Remove(invoice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                userDb.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}