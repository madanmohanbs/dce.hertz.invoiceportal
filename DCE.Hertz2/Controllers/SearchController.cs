﻿using DCE.Hertz2.DB;
using DCE.Hertz2.Models;
using DCE.Hertz2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;

namespace DCE.Hertz2.Controllers
{
    [Authorize(Roles="Admin")]
    public class SearchController : Controller
    {
        private HertzInvoiceDB db = new HertzInvoiceDB();
        private UsersContext userDb = new UsersContext();

        public ActionResult Invoice(string invoicenumber, string reservationnumber, string agreementnumber, int page=1, bool invoicedate = false, DateTime? startDate = null, DateTime? endDate = null )
        {
            var result = db.Invoices
                .Where(v => (invoicenumber == null || invoicenumber == "" || v.InvoiceNumber.StartsWith(invoicenumber))
                    && (reservationnumber == null || reservationnumber == "" || v.Reservation.StartsWith(reservationnumber))
                    && (agreementnumber == null || agreementnumber == "" || v.Reference.StartsWith(agreementnumber))
                    && (invoicedate == false) || (v.InvoiceDate >= startDate && v.InvoiceDate <= endDate)

                    );
                
              var model = result  
                .OrderBy(v => v.InvoiceNumber)
                .ToPagedList(page, 10);       
            
            if (Request.IsAjaxRequest())
            {
                return PartialView("_InvoiceResult", model);
            }

            return View(model);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Invoice( SearchInvoiceViewModel criteria)
        //{
        //    return View();
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                userDb.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
