﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using DCE.GeneratePdf.Models;
using System.Reflection;

namespace DCE.GeneratePdf
{
    class MergeFile
    {
        static public void Merge(InvoiceData data)
        {
            Stopwatch timer = Stopwatch.StartNew();
            Dictionary<string, string> invoice = data.InvoiceDetatail;
            List<Dictionary<string, string>> invoiceItems = data.InvoiceItems;
            string templateName = null;
            var app = new Microsoft.Office.Interop.Word.Application();
            if (invoice["Account"].StartsWith("9"))
            {
                templateName = @"C:\Dhamy\directpay.docx";
            }
            else
            {
                templateName = @"C:\Dhamy\hcc.docx";
            }

            var template = app.Documents.Add(Template: templateName);

            //app.Visible = true;

            //foreach (Microsoft.Office.Interop.Word.MailMergeField f in template.MailMerge.Fields)
            foreach (Microsoft.Office.Interop.Word.Field f in template.Fields)
            {
                if (f.Type == Microsoft.Office.Interop.Word.WdFieldType.wdFieldMergeField) { }
                //string mergeField = f.Code.Text;
                string mergeField = f.Result.Text;

                //foreach (string key in invoice.Keys)
                string key = mergeField.Substring(1, mergeField.Length - 2);

                if (invoice.Keys.Contains(key))
                {
                    //f.Select();
                    //app.Selection.TypeText(invoice[key] == string.Empty ? " " : invoice[key]);
                    f.Result.Text = invoice[key] == string.Empty ? " " : invoice[key];

                }


            }


            var cellOrder = new Dictionary<string, int> { { "RentalCharges", 1 }, { "Quantity", 2 }, { "Per", 3 }, { "Rate", 4 }, { "Subtotal", 5 } };

            if (invoiceItems != null)
            {
                // Very Bad Hard coded 4
                Microsoft.Office.Interop.Word.Table t = template.Tables[4];
                int rowCount = invoiceItems.Count;

                for (int i = 0; i < invoiceItems.Count; i++)
                {
                    t.Rows.Add(t.Rows[2]);
                }

                int rowNumber = 2;
                //int cellNumber;
                foreach (var item in invoiceItems)
                {
                    //cellNumber = 1;
                    foreach (var itemkey in item.Keys)
                    {
                        string itemcell = item[itemkey];
                        t.Cell(rowNumber, cellOrder[itemkey]).Range.InsertAfter(itemcell);
                        //cellNumber++;
                    }
                    rowNumber++;
                }
            }

            // Remove any left mail merge field.

            //Footer.........
            // Loop through all sections
            foreach (Microsoft.Office.Interop.Word.Section section in template.Sections)
            {
                //Get all Footers
                Microsoft.Office.Interop.Word.HeadersFooters footers = section.Footers;
                //Section headerfooter loop for all types enum WdHeaderFooterIndex. wdHeaderFooterEvenPages/wdHeaderFooterFirstPage/wdHeaderFooterPrimary; 
                foreach (Microsoft.Office.Interop.Word.HeaderFooter footer in footers)
                {
                    Microsoft.Office.Interop.Word.Fields fields = footer.Range.Fields;
                    foreach (Microsoft.Office.Interop.Word.Field field in fields)
                    {
                        if (field.Type == Microsoft.Office.Interop.Word.WdFieldType.wdFieldMergeField)
                        {
                            string mergeField = field.Code.Text;
                            foreach (string key in invoice.Keys)
                            {

                                if (mergeField.Contains(key))
                                {
                                    field.Select();
                                    //field.Delete();
                                    app.Selection.TypeText(invoice[key] == string.Empty ? " " : invoice[key]);
                                }
                            }
                        }
                    }
                }
            }



            template.ExportAsFixedFormat(@"C:\Dhamy\Files\" + invoice["InvoiceNumber"] + ".pdf", Microsoft.Office.Interop.Word.WdExportFormat.wdExportFormatPDF);
            //template.SaveAs(FileName: @"C:\Dhamy\geetha11.pdf");
            template.Saved = true;

            ((Microsoft.Office.Interop.Word._Document)template).Close();
            ((Microsoft.Office.Interop.Word._Application)app).Quit();

            timer.Stop();
            TimeSpan timespan = timer.Elapsed;
            Console.WriteLine(timespan.ToString());
            //Console.Read();

        }

        static public void Merge(Invoice data)
        {
            Stopwatch timer = Stopwatch.StartNew();

            var invoice = data;
            var invoiceItems = data.Charges;


            string templateName = null;
            var app = new Microsoft.Office.Interop.Word.Application();
            if (invoice.Account.StartsWith("9"))
            {
                templateName = @"C:\Dhamy\directpay.docx";
            }
            else
            {
                templateName = @"C:\Dhamy\hcc.docx";
            }

            var template = app.Documents.Add(Template: templateName);

            //app.Visible = true;

            //foreach (Microsoft.Office.Interop.Word.MailMergeField f in template.MailMerge.Fields)
            foreach (Microsoft.Office.Interop.Word.Field f in template.Fields)
            {
                if (f.Type == Microsoft.Office.Interop.Word.WdFieldType.wdFieldMergeField) { }
                //string mergeField = f.Code.Text;
                string mergeField = f.Result.Text;

                //foreach (string key in invoice.Keys)
                string key = mergeField.Substring(1, mergeField.Length - 2);

                var property = invoice.GetType().GetProperty(key, BindingFlags.Public | BindingFlags.Instance);
                if (property != null)
                {
                    //f.Select();
                    //app.Selection.TypeText(invoice[key] == string.Empty ? " " : invoice[key]);
                    var value = property.GetValue(invoice).ToString();
                    f.Result.Text = (value == string.Empty) ? " " : value;

                }


            }


            var cellOrder = new Dictionary<string, int> { { "RentalCharges", 1 }, { "Quantity", 2 }, { "Per", 3 }, { "Rate", 4 }, { "Subtotal", 5 } };

            if (invoiceItems != null)
            {
                // Very Bad Hard coded 4
                Microsoft.Office.Interop.Word.Table t = template.Tables[4];
                int rowCount = invoiceItems.Count;

                for (int i = 0; i < invoiceItems.Count; i++)
                {
                    t.Rows.Add(t.Rows[2]);
                }

                int rowNumber = 2;
                //int cellNumber;
                foreach (var item in invoiceItems)
                {
                    //cellNumber = 1;
                    foreach (var key in cellOrder.Keys)
                    {
                        var property = item.GetType().GetProperty(key, BindingFlags.Public | BindingFlags.Instance);
                        if (property != null)
                        {
                            string itemcell = property.GetValue(item).ToString();
                            t.Cell(rowNumber, cellOrder[property.Name]).Range.InsertAfter(itemcell);
                            //cellNumber++;
                        }
                    }
                    rowNumber++;
                }
            }

            // Remove any left mail merge field.

            //Footer.........
            // Loop through all sections
            foreach (Microsoft.Office.Interop.Word.Section section in template.Sections)
            {
                //Get all Footers
                Microsoft.Office.Interop.Word.HeadersFooters footers = section.Footers;
                //Section headerfooter loop for all types enum WdHeaderFooterIndex. wdHeaderFooterEvenPages/wdHeaderFooterFirstPage/wdHeaderFooterPrimary; 
                foreach (Microsoft.Office.Interop.Word.HeaderFooter footer in footers)
                {
                    Microsoft.Office.Interop.Word.Fields fields = footer.Range.Fields;
                    foreach (Microsoft.Office.Interop.Word.Field field in fields)
                    {
                        if (field.Type == Microsoft.Office.Interop.Word.WdFieldType.wdFieldMergeField)
                        {

                            string mergeField = field.Code.Text;
                            string key = mergeField.Substring(1, mergeField.Length - 2);
                            var property = invoice.GetType().GetProperty(key, BindingFlags.Public | BindingFlags.Instance);
                            if (property != null)
                            {


                                //field.Select();
                                //field.Delete();
                                //app.Selection.TypeText(invoice[key] == string.Empty ? " " : invoice[key]);
                                var value = property.GetValue(invoice).ToString();
                                field.Result.Text = (value == string.Empty) ? " " : value;
                            }
                        }
                    }
                }
            }



            template.ExportAsFixedFormat(@"C:\Dhamy\Files\" + invoice.InvoiceNumber + ".pdf", Microsoft.Office.Interop.Word.WdExportFormat.wdExportFormatPDF);
            //template.SaveAs(FileName: @"C:\Dhamy\geetha11.pdf");
            template.Saved = true;

            ((Microsoft.Office.Interop.Word._Document)template).Close();
            ((Microsoft.Office.Interop.Word._Application)app).Quit();

            timer.Stop();
            TimeSpan timespan = timer.Elapsed;
            Console.WriteLine(timespan.ToString());
            //Console.Read();

        }
    }
}