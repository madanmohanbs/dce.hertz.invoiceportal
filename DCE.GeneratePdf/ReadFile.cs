﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DCE.GeneratePdf
{
    class ReadFile
    {
        public static Dictionary<string, InvoiceData> Collect(string file)
        {
            var collection = new Dictionary<string, InvoiceData>();

            StreamReader reader = new StreamReader(file);

            int index = 0;
            string[] headings = new string[] { };
            while (!reader.EndOfStream)
            {
                var invoice = new Dictionary<string, string>();

                if (index == 0)
                {
                    var header = reader.ReadLine();
                    headings = header.Split(',');

                }
                index++;
                var line = reader.ReadLine();
                var items = line.Split(',');

                int itemCount = headings.Length;
                for (int i = 0; i < itemCount; i++)
                {
                    if (string.Empty != (headings[i]))
                    {
                        var key = String.Join("", headings[i].Split(' ', ':', '&', '-',';','/'));
                        invoice.Add(key, items[i]);
                    }
                }
                string invoiceNumber = invoice["InvoiceNumber"];
                var itemkeys = new List<string> { "RentalCharges", "Quantity", "Per", "Rate", "Subtotal" };
                var invoiceItems = itemkeys.Where(k => invoice.ContainsKey(k)).Select(k => new KeyValuePair<string, string>(k, invoice[k])).ToDictionary(k => k.Key, k => k.Value);


                if (!collection.ContainsKey(invoiceNumber))
                {
                    InvoiceData data = new InvoiceData { InvoiceDetatail = invoice };
                    data.InvoiceItems.Add(invoiceItems);
                    collection.Add(invoiceNumber, data);
                }
                else
                {
                    InvoiceData data = collection[invoiceNumber];
                    data.InvoiceItems.Add(invoiceItems);
                }

                //Console.WriteLine(index);
                //Console.WriteLine(String.Join(";", invoice.Keys));
                //Console.WriteLine(String.Join(";",invoice.Values));
                //Console.Read();


            }
            return collection;
        }
    }
}
