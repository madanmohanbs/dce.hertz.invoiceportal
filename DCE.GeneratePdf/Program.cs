﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCE.GeneratePdf
{
    class Program
    {
        static void Main(string[] args)
        {

            // var invoice = SampleInvoice();

            //PdfFromFile();
            PdfFromDB();

        }

        private static void PdfFromDB()
        {

            var collection = ReadData.Collect(@"C:\Dhamy\nz_invoice.csv");
            var data = collection.ToList();


            for (int i = 0; i < 2; i++)
            {
                MergeFile.Merge(data[i]);
            }
        }
        private static void PdfFromFile()
        {

            var collection = ReadFile.Collect(@"C:\Dhamy\nz_invoice.csv");
            var data = collection.Values.ToList();


            for (int i = 0; i < 200; i++)
            {
                MergeFile.Merge(data[i]);
            }
        }

        private static Dictionary<string, string> SampleInvoice()
        {
            var invoice =
                new Dictionary<string, string>{

                { "InvoiceNumber",	"50017355A"},
                {"Account",	"2965900"},
                {"ChargeTo", 	"PO BOX 992 SHORTLAND STREET"},
                {"Address2",	"AUCKLAND"},
                {"State", ""},
                {"Zip",	"1140"},
                {"Country","NZ"},
                {"InvoiceDate",	"5-Jan-15"},
                {"GstAmount",	"18.27"},
                {"Reference",	"500173553"},
                {"Reservation", 	"G3741654829"},
                {"OrderNumber",	"7073709"},
                {"VoucherNumber",""},	
                {"FreqFlyer",""},	
                {"PayType",	"OTTO 2965*9994"},
                {"RateCDP",	"9915 AA INSURANCE REPLACEMENT"},
                {"Hirer",	"LANFEAR WENDY"},
                {"PickUpLocation",	"NPL50 NEW PLYMOUTH AIRPORT"},
                {"PickUpDateTime",	"11/1/2014 12:20"},
                {"ReturnLocation",	"NPL50 NEW PLYMOUTH AIRPORT"},
                {"ReturnDateTime",	"11/8/2014 8:00"},
                {"VehicleCharged",	"D"},
                {"LicenceNo",	"HKT55 401998"},
                {"ReturnVehicle",	"BLUE FOCUS"},
                {"KMOut",	"5770"},
                {"KMIn",	"5922"},
                {"KMDriven", "234"},
                {"CarbonEmissions",	"22648"},
                {"Company",	"ENERGY CITY MOTORS LIMITED"},
                {"GstNo",	"11047793"},
                {"Currency",	"NZD" },
                {"GstPercent", "15%"},
                {"TotalCharges", "1234.00"},
                {"PaidAmount","134.00"},
                {"AmountDue","43.00"}

            };

            var invoiceItems = new List<Dictionary<string, string>>{ 
                new Dictionary<string, string> {
                    {"RentalCharges",	"Daily Charges"},
                    {"Per","D"},
                    {"Quantity",	"7"},
                    {"Rate",	"17.4"},
                    {"SubTotal",	"121.8"}
                },
                 new Dictionary<string, string> {
                    {"RentalCharges",	"Airport Fee"},
                    {"Per","U"},
                    {"Quantity",	"1"},
                    {"Rate",	"45"},
                    {"SubTotal",	"45"}
                }
            };
            return invoice;
        }

    }
}

