﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DCE.GeneratePdf.Models
{
     [Table("Charges")]
    public class Charges // : IValidatableObject
    {
        //[Range(1,1000)]
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        //[Required]
        //[MaxWords(2)]
        public string RentalCharges { get; set; }
        [Display(Name = "@")]
        public string Per { get; set; }
        [DisplayFormat(NullDisplayText = "#.00")]
        public decimal? Quantity { get; set; }
        public decimal? Rate { get; set; }
        public decimal? SubTotal { get; set; }

        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    if (Quantity * Rate < 1)
        //    {
        //        yield return new ValidationResult("Sub Total is too lower to include");
        //    }
        //}
    }
}