﻿

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Resources;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCE.GeneratePdf.Models
{

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Account { get; set; }
        public int? CustomerId { get; set; }
        public string Email { get; set; }


        public Customer Customer { get; set; }

    }
}
