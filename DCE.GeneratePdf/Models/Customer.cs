﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCE.GeneratePdf.Models
{
    [Table("Customer")]
    public class Customer
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string PayType { get; set; }
        public string Status { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Email {get; set;}
        public string Phone { get; set; }

        [NotMapped]
        public int? UsersCount { get; set; }
        
        public virtual ICollection<Invoice> Invoices { get; set; }
        public virtual ICollection<UserProfile> UserProfiles { get; set; }


    }
}